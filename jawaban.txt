1. Membuat database
create database myshop;

2. Membuat table
a. categories
create table categories(
    -> id int(8) auto_increment,
    -> name varchar(30),
    -> primary key(id)
    -> );
	
b. items
 create table items(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(8),
    -> stock int(8),
    -> categories_id int(8),
    -> primary key(id),
    -> foreign key(categories_id) references categories(id)
    -> );
	
c. users
create table users(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key(id)
    -> );
	
3. Memasukan Data
insert into items(name, description, price, stock, categories_id) values("Sumsang b50", "hape keren dari merek sumsang", "4000000", "100", 1);
insert into items(name, description, price, stock, categories_id) values("Uniklooh", "baju keren dari brank ternama", "500000", "50", 2);
insert into items(name, description, price, stock, categories_id) values("IMHO Watch", "jam tangan anak yang jujur banget", "2000000", "10", 1);

4. Mengambil Data
a. mengambil data kecuali categories_id
select id, name, name, description, price, stock from items;

b. mengambil data harga lebih dari 1 juta
select * from items where price > 1000000;	

c. Mengambil data nama serupa
select * from items where name like 'unik&';

5. menampilkan data join
select items.id, items.name, items.description, items.price, items.stock, items.categories_id, categories.name from items inner join categories on items.categories_id = categories.id;